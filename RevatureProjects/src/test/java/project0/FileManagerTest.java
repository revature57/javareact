package project0;

import org.junit.Before;
//import org.junit.Ignore;
import org.junit.Test;

public class FileManagerTest {
	FileManager manager;
	
	@Before 
	public void testInstantiate(){
		manager = new FileManager();
	}

//	public void testSaveDatabase() {
//		manager.userBase.put("adminUser:adminPass", new Admin());
//		manager.userBase.put("employUser:employPass", new Employee());
//		manager.saveAccountsToDatabase("src/main/resources/accounts.ser");
//		manager.saveUsersToDatabase("src/main/resources/users.ser");
//		manager.saveValidationsToDatabase("src/main/resources/validations.ser");
//	}
//	
	@Test
	public void testFileIO() {
		manager.loadAccountsToTerminal("src/main/resources/accounts.ser");
		manager.loadUsersToTerminal("src/main/resources/users.ser");
		manager.loadValidationsToTerminal("src/main/resources/validations.ser");
		System.out.println(manager.accountBase.entrySet());
		System.out.println(manager.userBase.entrySet());
		System.out.println(manager.validations);
		manager.saveAccountsToDatabase("src/main/resources/accounts.ser");
		manager.saveUsersToDatabase("src/main/resources/users.ser");
		manager.saveValidationsToDatabase("src/main/resources/validations.ser");
	}

//	public void testLoadUsers() {
//		manager.loadUsers("src/test/resources/usersTestRead.txt");
//		for (User usr:manager.userBase.values()) {
//			System.out.println(usr.stringify());
//		}
//	}

//	public void testSaveUsers() {
//		User testAdmin = new Admin();
//		User testEmployee = new Employee();
//		User testCustomer = new Customer();
//		manager.userBase.put(testAdmin.username+":"+testAdmin.password, testAdmin);
//		manager.userBase.put(testEmployee.username+":"+testEmployee.password, testEmployee);
//		manager.userBase.put(testCustomer.username+":"+testCustomer.password, testCustomer);
//		manager.saveUsers("src/test/resources/usersTest.txt");
//	}

//	public void testUserIO() {
//		manager.loadUsers("src/test/resources/usersTestRead.txt");
//		manager.saveUsers("src/test/resources/usersTest.txt");
//	}
//	

//	public void testLoadValidations() {
//		manager.loadValidations("src/test/resources/validationsTest.txt");
//		System.out.println(manager.validations);
//	}

//	public void testSaveValidations() {
//		manager.validations.add("test1");
//		manager.validations.add("test2");
//		manager.saveValidations("src/test/resources/validationsTest.txt");
//	}

//	public void testSaveAccounts() {
//		Account testAcc = new Account();
//		manager.accountBase.put(testAcc.getAccountID(), testAcc);
//		manager.saveAccounts("src/test/resources/accountsTest.txt");
//	}

//	public void testLoadAccounts() {
//		manager.loadAccounts("src/test/resources/accountsTest.txt");
//		System.out.println(manager.accountBase);
//	}
//
}
