package project0;

import static org.junit.Assert.*;

//import java.util.ArrayList;
//import java.util.Arrays;

import org.junit.Before;
//import org.junit.Ignore;
import org.junit.Test;

public class AccountTest {
	
	Account acc;
	
	@Before
	public void testInitialization() {
		acc = new Account();
	}
	

	@Test
	public void testWithdraw() {
		try {
			acc.deposit(50);
		} catch (NegativeDepositException e1) {			
			e1.printStackTrace();
		}
		try {
			acc.withdraw(25);
		} catch (NegativeDepositException | OverWithdrawalException e) {
			
			e.printStackTrace();
		}
		assertTrue(25d == acc.getBalance());
	}
	
	@Test
	public void testDeposit() {
		try {
			acc.deposit(8);
		} catch (NegativeDepositException e) {		
			e.printStackTrace();
		}
		assertTrue(8d == acc.getBalance());
	}
	

}
