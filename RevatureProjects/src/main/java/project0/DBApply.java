
package project0;

import java.sql.SQLException;

public class DBApply {
	
	public static void newAccount(BankingMenuer bank) {
		String choice = "0";
		
		System.out.println("What kind of account would you like to apply to?");
		System.out.println("1. Individual Checking");
		System.out.println("2. Joint Checking");
		
		choice = bank.input.nextLine();
		
		switch (choice) {
			case "1":
				int currentcustid = Integer.parseInt(((Customer) bank.u).getCustomerID());
			try {
				AccountDAO.getDAO().addAccount(bank, currentcustid);
			} catch (SQLException e) {
				System.out.println("Error communicating with database.");
				System.out.println("Account creation failed.");
				System.out.println("------------------------------------------------------------");
//				e.printStackTrace();
				return;
			}
				System.out.println("New account with maximum withdrawal limit of 5000 created.");
				System.out.println("Please wait for employee approval before seeing your account.");
				System.out.println("------------------------------------------------------------");
				break;
			case "2":
				int currcustid = Integer.parseInt(((Customer) bank.u).getCustomerID());
				System.out.println("Please enter the customer ID of your joint account owner:");
				try { 
					int targetcustid = Integer.parseInt(bank.input.nextLine());
					int newaccid = AccountDAO.getDAO().addAccount(bank, currcustid);
					AccountDAO.getDAO().addAccount(bank, targetcustid, newaccid);
				} catch (Exception e) {
					System.out.println("That customer does not exist, please try again.");
					System.out.println("------------------------------------------------------------");
					e.printStackTrace();
				}
				System.out.println("New account with maximum withdrawal limit of 5000 created.");
				System.out.println("Please wait for employee approval before seeing your account.");
				System.out.println("------------------------------------------------------------");
				break;
			default:
				System.out.println("Sorry, we don't understand that input. Please try again. ");
				System.out.println("Application process failed.");
				System.out.println("------------------------------------------------------------");
		}
	}

}
