package project0;

import java.sql.SQLException;
import java.util.HashSet;

public class DBTransfer {
	
	public static void transfer(BankingMenuer bank) {
		//loads and prints accounts available to customer
		System.out.println("Enter the account ID of the account you would like to transfer from: ");
		HashSet<Integer> accounts;
		try {
			accounts = CustomerDAO.getDAO().getAccounts(bank);
		} catch (SQLException e) {
			System.out.println("Database error while fetching accounts.");
			e.printStackTrace();
			return;		
		}
		
		System.out.println("Available accounts:");
		System.out.println("--------------------------------------------");
		accounts.forEach( accid -> System.out.println("AccountID  --> " + accid));
		//we need both the owner id and the target transfer accid
		int ownerAccid = 0;
		int targetAccid = 0;
		try {
			ownerAccid = Integer.parseInt(bank.input.nextLine());
			if (!AccountDAO.getDAO().accountExists(bank, ownerAccid)) {
				//checked if the owner account exists
				System.out.println("Account doesn't exist.");
				System.out.println("Transfer failed.");
				System.out.println("--------------------------------------------");
				return;
			}
			float balance = AccountDAO.getDAO().getBalance(bank, ownerAccid);
			System.out.println("Current balance: $" + balance);
			System.out.println("---------------------------------------------");
			System.out.println("Enter the amount to withdraw: ");
			float amount = Float.parseFloat(bank.input.nextLine());
			//withdraws amount from owner account
			AccountDAO.getDAO().withdraw(bank, ownerAccid, amount, true);
			System.out.println("Enter the account ID that you would like to transfer to: ");
			targetAccid = Integer.parseInt(bank.input.nextLine());
			if (!AccountDAO.getDAO().accountExists(bank, targetAccid)) {
				//checked if target account exists
				//if target account doesn't exist, then revert withdrawal on owner account
				AccountDAO.getDAO().deposit(bank, ownerAccid, amount);
				System.out.println("Account doesn't exist.");
				System.out.println("Transfer failed.");
				System.out.println("--------------------------------------------");
				return;
			}
			//deposits to the transfer account
			AccountDAO.getDAO().deposit(bank, targetAccid, amount);
			//logs the transaction to the database once all other actions were successful
			TransactionDAO.getDAO().logTransaction(bank, ownerAccid, "withdraw", amount);
			TransactionDAO.getDAO().logTransaction(bank, targetAccid, "deposit", amount);
		} catch (OverWithdrawalException e) {
			System.out.println("You are attempting to withdraw over your limit or more than your balance!");
			System.out.println("Withdrawal failed.");
			System.out.println("------------------------------------------------------------");
			return;
		} catch (Exception e) {
			Menus.unknownChoiceMessage();
			System.out.println("Transfer failed.");
			System.out.println("--------------------------------------------");
			e.printStackTrace();
			return;
		}
		
		System.out.println("Transfer successful!");
		System.out.println("--------------------------------------------");
	}

}
