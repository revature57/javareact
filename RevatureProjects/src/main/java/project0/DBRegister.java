package project0;

import java.sql.*;

public class DBRegister {
	
	public static void register(BankingMenuer bank) {
		
		String user;
		String pass;
		int employeeid=0;
		boolean isEmployee = false;
		
		System.out.println("Please enter desired username: ");
		user = bank.input.nextLine();
		System.out.println("Please enter desired password: ");
		pass = bank.input.nextLine();
		
		System.out.println("Are you an employee here? (y/n)");
		String choice = bank.input.nextLine();
		if (choice.equals("y")) {
			isEmployee = true;
			System.out.println("Please enter your employeeid.");
			try {
				employeeid = Integer.parseInt(bank.input.nextLine());
			} catch (Exception e) {
				System.out.println("Sorry, that input was not understood.");
				return;
			}
			
		} else if (choice.equals("n")) {
			System.out.println("Creating new customer account");
		} else {
			Menus.unknownChoiceMessage();
			System.out.println("-----------------------------------");
			return;
		}
		
		
		try {
			UserDAO usrDAO = UserDAO.getDAO();
			if (usrDAO.userExists(bank, user)){
				System.out.println("That username is already taken.");
				return;
			}

			if (isEmployee) {
				EmployeeDAO empDAO = EmployeeDAO.getDAO();
				if (!empDAO.validationIsFree(bank, employeeid)) {
					System.out.println("Invalid employee id.");
					return;
				}
			}
		} catch (SQLException e) {
			System.out.println("Database read error.");
			e.printStackTrace();
			return;
		}
		
		try {
			if (isEmployee) {
				EmployeeDAO empDAO = EmployeeDAO.getDAO();
				empDAO.addEmployee(bank, user, pass, employeeid);
				
			} else {
				CustomerDAO custDAO = CustomerDAO.getDAO();
				custDAO.addCustomer(bank, user, pass);
			}
		} catch (Exception e) {
			System.out.println("Database addition failed, please try again");
			e.printStackTrace();
			return;
		}
		
		
		
		System.out.println("Thank you for registering, please sign in.");
		return;
	}

}
