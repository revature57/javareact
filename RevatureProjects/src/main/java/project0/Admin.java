package project0;

import java.io.Serializable;
import java.util.ArrayList;


public class Admin extends Employee implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2549719007359345117L;
	private String adminValidation;
	
	public Admin() {
		this.username = "adminUser";
		this.password = "adminPass";
		this.adminValidation = "adminVal";
	}
	
	public Admin(String user, String pass, String adminVal) {
		this.username = user;
		this.password = pass;
		this.adminValidation = adminVal;
	}
	

	public String getAdminValidation() {
		return adminValidation;
	}

	public void setAdminValidation(String adminValidation) {
		this.adminValidation = adminValidation;
	}
	
	@Override
	public String stringify() {
		StringBuffer dataString = new StringBuffer();
				
		dataString.append(username + ":" + password + ",");
		dataString.append("Admin,");
		dataString.append(adminValidation+",");
		dataString.append("end\n");
		
		return dataString.toString();
		
	}
	
	public static User parseString(ArrayList<String> parsed) {
		String[] loginInfo = parsed.get(0).split(":");
		User admin = new Admin(loginInfo[0], loginInfo[1], parsed.get(2));
		return admin;
	}
	
}
