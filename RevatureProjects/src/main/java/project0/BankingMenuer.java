package project0;
import java.util.*;
import java.sql.*;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

public class BankingMenuer {
	// this class is the main banking menu loop
	// can switch between menus and take in input
	
	
	
	//instance data
	protected User u = null;
	protected FileManager files;
	Scanner input;
	Connection dbcon;
	
	//Logger
//	private static final Logger logger = LogManager.getLogger(BankingMenuer.class);
	
//	public void testLog() {
//		logger.info("Writing at info level.");
//	}
	
	public void loadDatabase() {
		this.files = new FileManager();
		this.files.loadAccountsToTerminal("src/main/resources/accounts.ser");
		this.files.loadValidationsToTerminal("src/main/resources/validations.ser");
		this.files.loadUsersToTerminal("src/main/resources/users.ser");
//		this.files.loadAccounts("src/main/resources/accounts.txt");
//		this.files.loadUsers("src/main/resources/users.txt");
//		this.files.loadValidations("src/main/resources/validations.txt");
		
	}
	
	public void proposeClean(boolean isTest) {
		// clean files for fresh start up of part 1 banking application
		System.out.println("Would you like to clean the database? (y/n)");
		if(isTest) {
			this.files.cleanDatabase();
		} else {			
			try {
				String choice = this.input.nextLine();
				if (choice.equals("y")) {
					this.files.cleanDatabase();
				}
			} catch (Exception e) {
				System.out.println("No scanner initialized for this bank.");
			}
		
		}
	}
	
	public void initializeScanner() {
		//initializes bank scanner
		this.input = new Scanner(System.in);
	}
	
	public void initializeConnection() {
		//initializes connection to be used throughout app
		try {
			this.dbcon = DatabaseManager.getConnection();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Database not reached, failure of app.");
			e.printStackTrace();
		}
	}
	
	public void closeConn() throws SQLException {
		//public method to close connection
		this.dbcon.close();
	}
			
	public static void main(String[] args) {
		//bank initialization statements
		BankingMenuer bank = new BankingMenuer();
		bank.initializeScanner();
		bank.initializeConnection();
		
		//for default files
//		bank.files = new FileManager();
//		bank.loadDatabase();
//		bank.proposeClean(false);
				
		
		// do not allow program to progress until there is a user logged in
		// or user wants to terminate without logging in, which will be returned
		// null user
		boolean exit = false;
		while (!exit) {
		
			bank.u = null;
			bank.u = Menus.login(bank);
//			bank.u = DatabaseManager.login(bank);
						
			if (bank.u instanceof Employee) {
				// employee portal
				Menus.employeeMenu(bank,bank.u);
			} else if (bank.u instanceof Customer) {
				//customer portal
				Menus.customerMenu(bank,bank.u);
			}
			//exit when person decides to continue without logging on
			if (bank.u == null) {
				exit = true;
			}
		
		}
		
		System.out.println("Thank you for using Max Bank. Have a nice day!");
		
		
		
		//closing all open connections and scanners as well as resetting daily amount withdrawn
		bank.input.close();
		try {
			AccountDAO.getDAO().resetWithdrawn(bank);
			bank.closeConn();
		} catch (SQLException e) {
			System.out.println("Error while closing database connection.");
//			e.printStackTrace();
		}
		
		
		//save all new changes to bank before exiting
//		bank.files.saveAccountsToDatabase("src/main/resources/accounts.ser");
//		bank.files.saveUsersToDatabase("src/main/resources/users.ser");
//		bank.files.saveValidationsToDatabase("src/main/resources/validations.ser");
		
		
		
	}
	
	
}
