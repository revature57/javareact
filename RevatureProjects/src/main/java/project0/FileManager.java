package project0;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class FileManager {
	// class that contains helper functions for reading and writing to files
	
	//database
	protected HashMap<String, User> userBase = new HashMap<String, User>();
	protected HashMap<String, Account> accountBase = new HashMap<String, Account>();
	protected ArrayList<String> validations = new ArrayList<String>();
//	protected HashMap<String, Account> pendingBase = new HashMap<String, Account>();
	

	
	public void cleanDatabase () {
							
		this.userBase = new HashMap<String, User>();
	
		User defaultCust = new Customer();
		User defaultAdmin = new Admin();
		User defaultEmployee = new Employee();
		
		this.userBase.put(defaultCust.username+":"+defaultCust.password, defaultCust);
		this.userBase.put(((Customer) defaultCust).getCustomerID(), defaultCust);
		this.userBase.put(defaultAdmin.username+":"+defaultAdmin.password, defaultAdmin);
		this.userBase.put(defaultEmployee.username+":"+defaultEmployee.password, defaultEmployee);
		
		
		this.accountBase = new HashMap<String, Account>();
		
		Account defaultAcc = new Account();
		
		this.accountBase.put(defaultAcc.getAccountID(), defaultAcc);
		

		this.validations = new ArrayList<String>();
		this.validations.add("e0");
		this.validations.add("a0");
		
	}
	
	public void saveUsersToDatabase(String path) {
		try {
	         FileOutputStream fileOut =
	         new FileOutputStream(path);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(this.userBase);
	         out.close();
	         fileOut.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
	
	public void saveValidationsToDatabase(String path) {
		try {
	         FileOutputStream fileOut =
	         new FileOutputStream(path);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(this.validations);
	         out.close();
	         fileOut.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
	
	public void saveAccountsToDatabase(String path) {
		try {
	         FileOutputStream fileOut =
	         new FileOutputStream(path);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         this.accountBase.values().forEach(acc -> acc.setWithdrawn(0f));
	         out.writeObject(this.accountBase);
	         out.close();
	         fileOut.close();   
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
	
	@SuppressWarnings("unchecked")
	public void loadUsersToTerminal(String path) {
		try {
	         FileInputStream fileIn = new FileInputStream(path);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         this.userBase = (HashMap<String, User>) in.readObject();
	         in.close();
	         fileIn.close();
	      } catch (Exception e) {}
	}
	
	@SuppressWarnings("unchecked")
	public void loadAccountsToTerminal(String path) {
		try {
	         FileInputStream fileIn = new FileInputStream(path);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         this.accountBase = (HashMap<String, Account>) in.readObject();
	         in.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         c.printStackTrace();
	         return;
	      }
	}
	
	@SuppressWarnings("unchecked")
	public void loadValidationsToTerminal(String path) {
		try {
	         FileInputStream fileIn = new FileInputStream(path);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         this.validations = (ArrayList<String>) in.readObject();
	         in.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         c.printStackTrace();
	         return;
	      }
	}
	
	
	@Deprecated
	public void loadValidations(String path) {
		try {
		      File validationsFile = new File(path);
		      Scanner myReader = new Scanner(validationsFile);
		      while (myReader.hasNextLine()) {
		        String data = myReader.nextLine();
//		        System.out.println(data);
		        this.validations.add(data);
		      }
		      myReader.close();
		    } catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
	
	@Deprecated
	public void saveValidations(String path) {
		try {
			File validationsFile = new File(path);
			PrintWriter writer = new PrintWriter(validationsFile);
			validations.forEach(str -> writer.println(str));
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@Deprecated
	public void loadUsers(String path) {
		try {
		      File usersFile = new File(path);
		      Scanner myReader = new Scanner(usersFile);
		      while (myReader.hasNextLine()) {
		        String data = myReader.nextLine();
//		        System.out.println(data);
		        ArrayList<String> parsed = new ArrayList<String>(Arrays.asList(data.split(",")));
		        
		        //handles the creation of user objects
//		        String[] loginInfo = parsed.get(0).split(":");
		        if (parsed.get(1).equals("Customer")) {			        	
		        	this.userBase.put(parsed.get(0), Customer.parseString(parsed));
		        } else if (parsed.get(1).equals("Admin")) {
		        	this.userBase.put(parsed.get(0), Admin.parseString(parsed));
		        } else if (parsed.get(1).equals("Employee")) {
		        	this.userBase.put(parsed.get(0),Employee.parseString(parsed));
		        }
		        
		      }
		      myReader.close();
		    } catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
	
	@Deprecated
	public void saveUsers(String path) {
		try {
			File usersFile = new File(path);
			PrintWriter writer = new PrintWriter(usersFile);
			for (User usr: userBase.values()) {
				writer.print(usr.stringify());
			}
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@Deprecated
	public void loadAccounts(String path) {
		try {
		      File accountsFile = new File(path);
		      Scanner myReader = new Scanner(accountsFile);
		      while (myReader.hasNextLine()) {
		        String data = myReader.nextLine();
//		        System.out.println(data);
		        ArrayList<String> parsed = new ArrayList<String>(Arrays.asList(data.split(",")));
		        this.accountBase.put(parsed.get(0), Account.parseString(parsed));
		      }
		      myReader.close();
		    } catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
	
	@Deprecated
	public void saveAccounts(String path) {
		try {
			File accountsFile = new File(path);
			PrintWriter writer = new PrintWriter(accountsFile);
			for (Account acc: accountBase.values()) {
				writer.print(acc.stringify());
				
			}
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	
}
