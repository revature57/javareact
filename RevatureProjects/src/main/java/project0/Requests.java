package project0;

import java.util.ArrayList;
import java.util.Arrays;


public class Requests {

	public static float custDepositMenu(BankingMenuer bank,User u) {
		
		int choice = 0;
		float amount = 0;
		
		System.out.println("To which account would you like to deposit?");
		int numberOfChoices = 0;
		
		for (int i = 0; i<((Customer) u).getAccounts().size();i++) {
			//print only approved accounts
			if (bank.files.accountBase.get(((Customer) u).getAccounts().get(i)).isPending()) {
				continue;
			} else {
				numberOfChoices++;
				String accID = ((Customer) u).getAccounts().get(i);
				Account loopedAcc = bank.files.accountBase.get(accID);
				System.out.println((numberOfChoices)+". "+accID+ " - Balance: $" + loopedAcc.getBalance());
			}
		}
		
		try {
			choice = Integer.parseInt(bank.input.nextLine());
			if (choice > numberOfChoices || choice < 0 ) {
				//invalid account choice
				System.out.println("That's not a valid account");
				System.out.println("---------------------------------------------------");
			} else {
				Account acc = bank.files.accountBase.get(((Customer) u).getAccounts().get(choice-1));
				//queries the user for amount to deposit and passes the request to account
				System.out.println("Current balance in account: $" + acc.getBalance());
				System.out.println("How much would you like to deposit?");
				amount = Float.parseFloat(bank.input.nextLine());
				try {
					//sends the deposit request
					acc.deposit(amount);
				} catch (NegativeDepositException e) {
					// user tries to deposit a negative account
					System.out.println("You can't deposit a negative amount!");
					System.out.println("Deposit failed.");
					System.out.println("------------------------------------------------------------");
				}
			}
		} catch (Exception e) {
			//any other input error
			Menus.unknownChoiceMessage();
			System.out.println("Deposit failed.");
			System.out.println("------------------------------------------------------------");
		}
		
		return amount;
	}

	public static ArrayList<Object> custWithdrawMenu(BankingMenuer bank,User u) {
		int choice = 0;
		float amount = 0f;
		Account acc = null;
		
		System.out.println("From which account would you like to withdraw?");
		int numberOfChoices = 0;
		
		for (int i = 0; i<((Customer) u).getAccounts().size();i++) {
			//print only approved accounts
			if (bank.files.accountBase.get(((Customer) u).getAccounts().get(i)).isPending()) {
				continue;
			} else {
				numberOfChoices++;
				String accID = ((Customer) u).getAccounts().get(i);
				Account loopedAcc = bank.files.accountBase.get(accID);
				System.out.println((numberOfChoices)+". "+accID+ " - Balance: $" + loopedAcc.getBalance());
			}
		}
		
		try {
			choice = Integer.parseInt(bank.input.nextLine());
			if (choice > numberOfChoices || choice < 0 ) {
				//invalid account choice
				System.out.println("That's not a valid account");
				System.out.println("---------------------------------------------------");
			} else {
				acc = bank.files.accountBase.get(((Customer) u).getAccounts().get(choice-1));
				//queries the user for amount to deposit and passes the request to account
				System.out.println("Current balance in account: $" + acc.getBalance());
				System.out.println("Account withdrawal limit: $" + acc.getMaxWithdrawal());
				System.out.println("Withdrawn in current session: $" + acc.getWithdrawn());
				System.out.println("How much would you like to withdraw?");
				amount = Float.parseFloat(bank.input.nextLine());
				try {
					//sends the withdrawal request
					acc.withdraw(amount);
				} catch (NegativeDepositException e) {
					// user tries to deposit a negative account
					System.out.println("You can't withdraw a negative amount!");
					System.out.println("Withdrawal failed.");
					System.out.println("---------------------------------------------------");
				} catch (OverWithdrawalException e) {
					System.out.println("You are attempting to withdraw over your limit or more than your balance!");
					System.out.println("Withdrawal failed.");
					System.out.println("------------------------------------------------------------");
				}
			}
		} catch (Exception e) {
			//any other input error
			Menus.unknownChoiceMessage();
			System.out.println("Withdrawal failed.");
			System.out.println("---------------------------------------------------");
		} 
		
		return new ArrayList<Object> (Arrays.asList(acc, amount));
		
	}

	public static void custTransferMenu(BankingMenuer bank,User u) {
		ArrayList<Object> withdrawnAccInfo = custWithdrawMenu(bank, u);
		float transferAmount = (float) withdrawnAccInfo.get(1);
		Account withdrawnAcc = (Account) withdrawnAccInfo.get(0);
		
		if (transferAmount <= 0) {
			System.out.println("No amount to transfer, going back to main menu...");
		} else {
			System.out.println("Please enter the account ID of the account you would like to transfer to: ");
			try {
			String transferID = bank.input.nextLine();
			Account transferAcc = bank.files.accountBase.get(transferID);
				try {
					transferAcc.deposit(transferAmount);
				} catch (NegativeDepositException e) {
					System.out.println("You can't deposit a negative amount!");
					System.out.println("Deposit failed.");
					System.out.println("------------------------------------------------------------");
				}
			} catch (Exception e) {
				System.out.println("Account ID entered invalid.");
				System.out.println("Transfer failed.");
				System.out.println("------------------------------------------------------------");
				try {
					withdrawnAcc.deposit(transferAmount);
				} catch (NegativeDepositException e1) {
					System.out.println("You can't deposit a negative amount!");
					System.out.println("Deposit failed.");
					System.out.println("------------------------------------------------------------");
				}
			}
		}
		
	}

	public static void custApplyMenu(BankingMenuer bank,User u) {
		
		String choice = "0";
		
		System.out.println("What kind of account would you like to apply to?");
		System.out.println("1. Individual Checking");
		System.out.println("2. Joint Checking");
		
		choice = bank.input.nextLine();
		
		switch (choice) {
			case "1":
				String newAccID = "accID" + Integer.toString(bank.files.accountBase.size());
				Account newAcc = new Account(newAccID, 0f, 500, true);
				bank.files.accountBase.put(newAccID, newAcc);
				((Customer) u).getAccounts().add(newAccID);
				System.out.println("New account with maximum withdrawal limit of 500 created.");
				System.out.println("Please wait for employee approval before seeing your account.");
				System.out.println("------------------------------------------------------------");
				break;
			case"2":
				System.out.println("Please enter the customer ID of your joint account owner:");
				String jointID = bank.input.nextLine();
							
				if (bank.files.userBase.keySet().contains(jointID)) {
					//creates the new account
					User jointUser = bank.files.userBase.get(jointID);
					String newJointAccID = "accID" + Integer.toString(bank.files.accountBase.size());
					Account newJointAcc = new Account(newJointAccID, 0f, 500, true);
					bank.files.accountBase.put(newJointAccID, newJointAcc);
					//adds the new account id to the current customer and the customer they wanted
					//to create the joint account with
					((Customer) u).getAccounts().add(newJointAccID);
					((Customer) jointUser).getAccounts().add(newJointAccID);
					//outputs success
					System.out.println("New account with maximum withdrawal limit of 500 created.");
					System.out.println("Please wait for employee approval before seeing your account.");
					System.out.println("------------------------------------------------------------");
				} else {
					System.out.println("That customer does not exist. Please try again.");
					System.out.println("------------------------------------------------------------");
				}
				break;
			default:
				System.out.println("Sorry, we don't understand that input. Please try again. ");
				System.out.println("Application process failed.");
				System.out.println("------------------------------------------------------------");
				
		}
	}

	public static void empViewAccMenu(BankingMenuer bank,User u) {
		System.out.println("Enter the account ID that you wish to view: ");
		String accID = bank.input.nextLine();
		Account acc = null;
		boolean accExists = bank.files.accountBase.keySet().contains(accID);
		if (accExists) {
			acc = bank.files.accountBase.get(accID);
			System.out.println("Account Information: ");
			System.out.println("---------------------------------------------------");
			System.out.println("Account ID: " + acc.getAccountID());
			System.out.println("Balance: $" + acc.getBalance());
			System.out.println("Account maximum daily withdrawal: " + acc.getMaxWithdrawal());
			System.out.println("---------------------------------------------------");
		} else {
			System.out.println("That account does not exist");
			System.out.println("---------------------------------------------------");
		}
		if (u instanceof Admin && accExists) {
			System.out.println("What would you like to do with this account?");
			System.out.println("---------------------------------------------------");
			System.out.println("1. Edit account balance");
			System.out.println("2. Edit account maximum daily withdrawal");	
			System.out.println("3. Delete this account");	
			String choice = bank.input.nextLine();
			
			switch (choice) {
			case "1":
				System.out.println("Please enter the new balance:");
				String newBalance = bank.input.nextLine();
				try {
					acc.setBalance(Float.parseFloat(newBalance));
				} catch (NegativeDepositException e) {
					System.out.println("You can't have a negative balance!");
					System.out.println("Editing failed. Please try again.");
					System.out.println("---------------------------------------------------");
				} catch (Exception e) {
					System.out.println("Input not understood.");
					System.out.println("Editing failed. Please try again.");	
					System.out.println("---------------------------------------------------");
				}
				break;
			case "2":
				System.out.println("Please enter the new maximum daily withdrawal limit:");
				String newLimit = bank.input.nextLine();
				try {
					acc.setMaxWithdrawal(Float.parseFloat(newLimit));
				} catch (NegativeDepositException e) {
					System.out.println("You can't have a negative withdrawal limit!");
					System.out.println("Editing failed. Please try again.");
					System.out.println("---------------------------------------------------");
				} catch (Exception e) {
					System.out.println("Input not understood.");
					System.out.println("Editing failed. Please try again.");	
					System.out.println("---------------------------------------------------");
				}
				break;
			case "3":
				bank.files.accountBase.remove(accID);
				System.out.println("Account deleted successfully.");
				System.out.println("---------------------------------------------------");
				break;
			default:
				Menus.unknownChoiceMessage();
			}
		}
		
	}

	public static void empViewPendMenu(BankingMenuer bank,User u) {
		System.out.println("These are the pending accounts: ");
		System.out.println("---------------------------------------------------");
		for (Account acc: bank.files.accountBase.values()) {
			if (acc.isPending()) {
				System.out.println("Account ID: "+acc.getAccountID());
			}
		}
		System.out.println("---------------------------------------------------");
		System.out.println("Enter an account ID to approve: ");
		String accID = bank.input.nextLine();
		if (bank.files.accountBase.keySet().contains(accID)) {
			bank.files.accountBase.get(accID).setPending(false);
		} else {
			System.out.println("Account doesn't exist.");
		}
	}
	
	public static void empViewCustsMenu(BankingMenuer bank,User u) {
		System.out.println("Please enter the customer ID: ");
		String custID = bank.input.nextLine();
		User cust = null;
		if (bank.files.userBase.keySet().contains(custID)) {
			cust = bank.files.userBase.get(custID);
			System.out.println("Customer Information: ");
			System.out.println("---------------------------------------------------");
			System.out.println("Customer ID: " + ((Customer) cust).getCustomerID());
			System.out.println("Customer username: " + cust.getUsername());
			System.out.println("Customer password: " + cust.getPassword());
			System.out.println("Customer Accounts: " + ((Customer) cust).getAccounts());
			if (u instanceof Admin) {
				System.out.println("What would you like to do to this user?");
				System.out.println("---------------------------------------------------");
				System.out.println("1. Edit username");
				System.out.println("2. Edit password");
				System.out.println("3. Remove an account");
				System.out.println("4. Add an account");
				String choice = bank.input.nextLine();
				switch (choice) {
				case "1":
					System.out.println("Enter new username: ");
					String newUsername = bank.input.nextLine();
					boolean usernameAvailable = true;
					for (User usr:bank.files.userBase.values()) {
						if (usr.getUsername().equals(newUsername)) {
							usernameAvailable = false;
							break;
						}
					}
					if (usernameAvailable) {
						cust.setUsername(newUsername);
						System.out.println("New username set.");
						System.out.println("---------------------------------------------------");
					} else {
						System.out.println("That username is already taken.");
						System.out.println("Setting new username failed.");
						System.out.println("---------------------------------------------------");
					}
					break;
				case "2":
					System.out.println("Enter new password: ");
					String newPassword = bank.input.nextLine();
					cust.setPassword(newPassword);
					System.out.println("New password set.");
					break;
				case "3":
					System.out.println("Enter the account ID to remove: ");
					String accID = bank.input.nextLine();
					if (((Customer) cust).getAccounts().contains(accID)) {
						((Customer) cust).getAccounts().remove(accID);
						System.out.println("Account removed successfully.");
						System.out.println("---------------------------------------------------");
					} else {
						System.out.println("Account does not belong to this customer.");
						System.out.println("No action taken.");
						System.out.println("---------------------------------------------------");
					}
				case "4":
					System.out.println("Enter the account ID to add: ");
					String accIDtoAdd = bank.input.nextLine();
					if (bank.files.accountBase.keySet().contains(accIDtoAdd)) {
						((Customer) cust).getAccounts().add(accIDtoAdd);
						System.out.println("Account added successfully");
						System.out.println("---------------------------------------------------");
					} else {
						System.out.println("Account does not exist.");
						System.out.println("Account addition failed.");
						System.out.println("---------------------------------------------------");
					}
				}
			}
		} 
		
	}
	
	public static void empAddValMenu(BankingMenuer bank, User u) {
		if (u instanceof Admin) {
			System.out.println("Enter validation id: ");
			System.out.println("Employee ids start with e, and admins start with a. ");
			String newVal = bank.input.nextLine();
			if (newVal.charAt(0) == 'a' || newVal.charAt(0) == 'e') {
				bank.files.validations.add(newVal);
				System.out.println("Validation added successfully.");
				System.out.println("-----------------------------------------------");
			} else {
				System.out.println("That is not an acceptable validation code.");
				System.out.println("Validation addition failed.");
				System.out.println("-----------------------------------------------");
			} 
		} else {
			System.out.println("Sorry, only administrators have permission to add new validations.");
		}
	}

	public static void register(BankingMenuer bank, User u) {
		
		String user;
		String pass;
		boolean exit = false;
		boolean failed = false;
		while (!exit) {
			
			System.out.println("Please enter desired username: ");
			user = bank.input.nextLine();
			System.out.println("Please enter desired password: ");
			pass = bank.input.nextLine();
			for(User usr:bank.files.userBase.values()) {
				if (usr.getUsername().equals(user)) {
					System.out.println("This username already exists, please try again.");
					System.out.println("------------------------------------------------------------");
					failed = true;
					exit = true;
					break;
				}
			}
			
			if (!failed) {
				
				
				System.out.println("Do you have an employee code? (y/n)");
				String confirmation;
				boolean exitConfirm = false;
				while (!exitConfirm) {
					confirmation = bank.input.nextLine();
					switch (confirmation) {
						case "y":
							System.out.println("Enter employee code:");
							String code = bank.input.nextLine();
							if (bank.files.validations.contains(code)) {
								if (code.charAt(0) == ('a')){
									bank.files.userBase.put(user+":"+pass, new Admin(user, pass, code));
								} else {
									bank.files.userBase.put(user+":"+pass, new Employee(user, pass, code));
								}
								bank.files.validations.remove(code);
							} else {
								System.out.println("Validation code not found. Registration failed. Please try again.");
							}
							exitConfirm=true;
							break;
						case "n":
							Customer newCust = new Customer(user, pass, String.valueOf(bank.files.userBase.size()));
							bank.files.userBase.put(user+":"+pass, newCust);
							bank.files.userBase.put(newCust.getCustomerID(), newCust);
							exitConfirm= true;
							break;
						default:
							Menus.unknownChoiceMessage();
					}
				}
				System.out.println("Sign up success, redirecting....");
				exit=true;
			}
			
			
		}
		
		
	}

}
