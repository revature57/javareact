package project0;

import java.sql.SQLException;
import java.util.HashSet;

public class DBDeposit {
	
	public static void deposit(BankingMenuer bank) {
		//loads and prints the accounts associated to the customer
		HashSet<Integer> accounts;
		try {
			accounts = CustomerDAO.getDAO().getAccounts(bank);
		} catch (SQLException e) {
			System.out.println("Database error while fetching accounts.");
			e.printStackTrace();
			return;			
		}
		System.out.println("Available accounts:");
		System.out.println("--------------------------------------------");
		accounts.forEach( accid -> System.out.println("AccountID  --> " + accid));
		System.out.println("Enter the account ID to which you would like to deposit:");
		int accid = 0;
		try {
			//gets amount to deposit and makes the deposit
			accid = Integer.parseInt(bank.input.nextLine());
			float balance = AccountDAO.getDAO().getBalance(bank, accid);
			System.out.println("Current balance: $" + balance);
			System.out.println("---------------------------------------------");
			System.out.println("Enter the amount to deposit: ");
			float amount = Float.parseFloat(bank.input.nextLine());
			//deposit call
			AccountDAO.getDAO().deposit(bank, accid, amount);
			//logs the deposit
			TransactionDAO.getDAO().logTransaction(bank, accid, "deposit", amount);
		} catch (SQLException e) {
			System.out.println("Error while communicating with database.");
			System.out.println("Deposit failed.");
			System.out.println("--------------------------------------------");
			return;
		} catch (NegativeDepositException e) {
			System.out.println("You can't deposit a negative amount!");
			System.out.println("Deposit failed.");
			System.out.println("--------------------------------------------");
			return;
		} catch (Exception e) {
			Menus.unknownChoiceMessage();
			System.out.println("Deposit failed.");
			System.out.println("--------------------------------------------");
			return;
		}
		
		System.out.println("Deposit successful!");
		try {
			System.out.println("New balance: $"+AccountDAO.getDAO().getBalance(bank, accid));
		} catch (SQLException e) {
			System.out.println("Error displaying new balance.");
		}
		System.out.println("--------------------------------------------");
		
	}

}
