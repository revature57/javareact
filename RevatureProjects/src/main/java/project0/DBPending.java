package project0;
import java.util.HashSet;

import project0.AccountDAO;
import project0.BankingMenuer;
import project0.Menus;

public class DBPending {
	
	public static void viewPending(BankingMenuer bank) {
		HashSet<Integer> pendingAccs = null;
		int accid = 0;
		System.out.println("These are the pending accounts: ");
		System.out.println("---------------------------------------------------");
		try {
			pendingAccs = AccountDAO.getDAO().getPending(bank);
		} catch (Exception e) {
			System.out.println("Error occurred while communicating with database.");
			return;
		}
		pendingAccs.forEach(id -> System.out.println("Account ID --> "+id));
		System.out.println("---------------------------------------------------");
		System.out.println("Enter an account ID to approve: ");
		try {
			accid = Integer.parseInt(bank.input.nextLine());
			EmployeeDAO.getDAO().approve(bank, accid);
		} catch (Exception e) {
			Menus.unknownChoiceMessage();
//			e.printStackTrace();
			return;
		}
		System.out.println("Account approved successfully.");
		System.out.println("---------------------------------------------------");
	}

}
