package project0;

import java.sql.*;
import java.util.HashSet;

public class AccountDAO {
	
	private static AccountDAO singleton = null;
	
	public static AccountDAO getDAO() {
		
		if (singleton == null) {
			singleton = new AccountDAO();
		}
		
		return singleton;
	}
	
	public float getBalance(BankingMenuer bank, Integer accID) throws SQLException {
		float balance=0f;
		
		PreparedStatement ps = bank.dbcon.prepareStatement("SELECT balance FROM accounts WHERE accountid=?");
		ps.setInt(1, accID);
		ResultSet rs = ps.executeQuery();
		if (!rs.next()) {
			System.out.println("account does not exist");
			throw new SQLException();
		} else {
			balance = rs.getFloat("balance");
		}
		rs.close();
		ps.close();
		return balance; 
	}
	
	public void setBalance(BankingMenuer bank, Integer accID, float amount) throws SQLException {
		
		PreparedStatement ps = bank.dbcon.prepareStatement("UPDATE accounts SET balance = ? WHERE accountid=?");
		ps.setFloat(1, amount);
		ps.setInt(2, accID);
		ps.executeUpdate();
		ps.close();

	}
	
	public void deposit(BankingMenuer bank, Integer accID, float amount) throws NegativeDepositException, SQLException {
		if (amount < 0 ) {
			throw new NegativeDepositException("Tried to deposit a negative amount");
		}
		
		float balance = this.getBalance(bank, accID);
		balance+=amount;
		this.setBalance(bank, accID, balance);
		
	}
	
	public void withdraw(BankingMenuer bank, Integer accID, float amount, boolean isTransfer) throws NegativeDepositException, OverWithdrawalException, SQLException {
		float balance = this.getBalance(bank, accID);
		float withdrawn = this.getWithdrawn(bank, accID);
		float maxWithdrawal = this.getMaxWithdrawal(bank, accID);
		
		if (amount < 0) {
			throw new NegativeDepositException("Tried to withdraw negative amount!");
		} else if ( amount > balance || amount + withdrawn > maxWithdrawal) {
			throw new OverWithdrawalException("Tried to withdraw too much!");
		}
		
		balance-=amount;
		withdrawn+=amount;
		this.setBalance(bank, accID, balance);
		if (!isTransfer) {
			this.setWithdrawn(bank, accID, withdrawn);
		}
	}
	
	public void setWithdrawn(BankingMenuer bank, Integer accID, float amount) throws SQLException {
		
		PreparedStatement ps = bank.dbcon.prepareStatement("UPDATE accounts SET withdrawn = ? WHERE accountid=?");
		ps.setFloat(1, amount);
		ps.setInt(2, accID);
		ps.executeUpdate();
		ps.close();
		
	}

	public float getMaxWithdrawal(BankingMenuer bank, Integer accID) throws SQLException {
		float maxWithdrawal=0f;
		
		PreparedStatement ps = bank.dbcon.prepareStatement("SELECT maxwithdrawal FROM accounts WHERE accountid=?");
		ps.setInt(1, accID);
		ResultSet rs = ps.executeQuery();
		if (!rs.next()) {
			System.out.println("account does not exist");
			throw new SQLException();
		} else {
			maxWithdrawal = rs.getFloat("maxwithdrawal");
		}
		rs.close();
		ps.close();
		return maxWithdrawal; 
	}

	public float getWithdrawn(BankingMenuer bank, Integer accID) throws SQLException {
		float withdrawn=0f;
		
		PreparedStatement ps = bank.dbcon.prepareStatement("SELECT withdrawn FROM accounts WHERE accountid=?");
		ps.setInt(1, accID);
		ResultSet rs = ps.executeQuery();
		if (!rs.next()) {
			System.out.println("account does not exist");
			throw new SQLException();
		} else {
			withdrawn = rs.getFloat("withdrawn");
		}
		rs.close();
		ps.close();
		return withdrawn; 
	}

	public int count(BankingMenuer bank) throws SQLException {
		int count=0;
		
		Statement stmnt = bank.dbcon.createStatement();
		ResultSet rs = stmnt.executeQuery("SELECT COUNT(accountid) FROM accounts");
		if (!rs.next()) {
			throw new SQLException();
		} else {
			count = rs.getInt("count(accountid)");
		}
		
		rs.close();
		stmnt.close();
		return count;
	}
	
	public int addAccount(BankingMenuer bank, int custid) throws SQLException {
		int newaccid=0;
		newaccid = this.count(bank);
		Statement stmnt = bank.dbcon.createStatement();
		stmnt.addBatch("INSERT INTO accounts (accountid, balance, withdrawn, maxwithdrawal) VALUES (\'"+newaccid+"\', DEFAULT, DEFAULT, DEFAULT)");
		stmnt.addBatch("INSERT INTO cust_account (custid, accountid, approved) VALUES (\'"+custid+"\',\'"+newaccid+"\', DEFAULT)");
		stmnt.executeBatch();
		stmnt.close();
		return newaccid;
		
	}
	
	public int addAccount(BankingMenuer bank, int custid, int newaccid) throws SQLException {
		Statement stmnt = bank.dbcon.createStatement();
		stmnt.addBatch("INSERT INTO cust_account (custid, accountid, approved) VALUES (\'"+custid+"\',\'"+newaccid+"\', DEFAULT)");
		stmnt.executeBatch();
		stmnt.close();
		return newaccid;
		
	}
	
	public boolean accountExists(BankingMenuer bank, int accid) throws SQLException {
		boolean exists = false;
		
		PreparedStatement ps = bank.dbcon.prepareStatement("SELECT COUNT(accountid) FROM accounts WHERE accountid=?");
		ps.setInt(1, accid);
		ResultSet rs = ps.executeQuery();
		if (!rs.next()) {
			throw new SQLException();
		} else {
			if (rs.getInt("count(accountid)") > 0) {
				exists = true;
			} else {
				exists = false;
			}
		}
		
		rs.close();
		ps.close();
		return exists;
	}

	public void setMaxWithdrawal(BankingMenuer bank, int accID, float amount) throws SQLException {
		PreparedStatement ps = bank.dbcon.prepareStatement("UPDATE accounts SET maxwithdrawal = ? WHERE accountid=?");
		ps.setFloat(1, amount);
		ps.setInt(2, accID);
		ps.executeUpdate();
		ps.close();
	}
	
	public void removeAcc(BankingMenuer bank, int accid) throws SQLException {
		
		Statement stmnt = bank.dbcon.createStatement();
		stmnt.addBatch("DELETE FROM accounts WHERE accountid = \'"+accid+"\'");
		stmnt.addBatch("DELETE FROM cust_account WHERE accountid = \'"+accid+"\'");
		stmnt.executeBatch();
		stmnt.close();
		
	}
	
	public HashSet<Integer> getPending(BankingMenuer bank) throws SQLException {
		
		HashSet<Integer> accs = new HashSet<Integer>();
		Statement stmnt = bank.dbcon.createStatement();
		ResultSet rs = stmnt.executeQuery("SELECT accountid FROM cust_account WHERE approved = 0");
		if (!rs.next()) {
			return accs;
		} else {
			do {
				accs.add(rs.getInt("accountid"));
			} while (rs.next());
		}
		
		return accs;
	}
	
	public void resetWithdrawn(BankingMenuer bank) throws SQLException {
		String resetUpdate = "UPDATE accounts SET withdrawn = 0";
		Statement stmnt = bank.dbcon.createStatement();
		stmnt.executeUpdate(resetUpdate);
		stmnt.close();
	}
	
	
}
