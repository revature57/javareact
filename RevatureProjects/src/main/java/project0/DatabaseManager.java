package project0;

import java.sql.*;

public class DatabaseManager {
	
	static final String user = "admin";
	static final String pass = "training!Rev12";
	static final String host = "jdbc:oracle:thin:@prject0.chrggyycmmyx.us-east-2.rds.amazonaws.com:1521:ORCL";

	public static void main() {
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			Connection connect = DriverManager.getConnection(host, user, pass);
			
			Statement stmt = connect.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM accounts");
			
			while(rs.next()) {
				System.out.println("balance: " + rs.getFloat("balance") +
						" accID: " + rs.getString("accountid") + 
						" withdrawn: " + rs.getFloat("withdrawn") +
						" maxWithdrawal: " + rs.getFloat("maxwithdrawal") +
						" isPending: " + rs.getInt("ispending")) ;
			}
			
			rs.close();
			stmt.close();
			connect.close();
			
			
		} catch (ClassNotFoundException e) {
			System.out.println("Unable to load driver class for database.");
			
		} catch (SQLException e) {
			System.out.println("SQL Exception.");
			e.printStackTrace();
		}
		

	}
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		Connection connect = DriverManager.getConnection(host, user, pass);
		
		return connect;
	}
	
	public static User login(BankingMenuer bank,String username, String password) throws ClassNotFoundException, SQLException {
		User usr=null;
		
		
		Connection connect = bank.dbcon;
		
		UserDAO usrDAO = UserDAO.getDAO();
		if (!usrDAO.userExists(bank, username)) {
			System.out.println("User does not exist. Please try again.");
			return usr;
		}
				
		String loginStatement = "SELECT * FROM users WHERE username=?";
		
		PreparedStatement getLoginInfo = connect.prepareStatement(loginStatement);
		getLoginInfo.setString(1, username);
		
		ResultSet userInfo = getLoginInfo.executeQuery();
		
		String dbPassword = null;
		String dbType = null;
		
		while(userInfo.next()) {
			dbPassword = userInfo.getString("password");
			dbType = userInfo.getString("usertype");
		}
		
		if (dbPassword.equals(password)) {
			if (dbType.equals("c")) {
				CustomerDAO custDAO = CustomerDAO.getDAO();
				Integer custid = custDAO.getCustID(connect, username);
				usr = new Customer(username, password, custid.toString());
			} else if (dbType.equals("a")) {
				EmployeeDAO empDAO = EmployeeDAO.getDAO();
				Integer adminid = empDAO.getEmpID(connect, username);
				usr = new Admin(username, password, adminid.toString());
			}  else if (dbType.equals("e")) {
				EmployeeDAO empDAO = EmployeeDAO.getDAO();
				Integer empid = empDAO.getEmpID(connect, username);
				usr = new Employee(username, password, empid.toString());
			}
		}
		
		userInfo.close();
		getLoginInfo.close();
		
		return usr;
	}

}
