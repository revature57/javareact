package project0;

public class NegativeDepositException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NegativeDepositException (String errorMessage) {
		super(errorMessage);
	}
	
}
