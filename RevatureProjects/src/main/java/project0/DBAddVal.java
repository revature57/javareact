package project0;

import java.sql.SQLException;

public class DBAddVal {
	public static void addVal(BankingMenuer bank) {
		int newID = 0;
		if (bank.u instanceof Admin) {
		try {
			System.out.println("Enter a new employee ID number (no characters or spaces allowed): ");
			newID = Integer.parseInt(bank.input.nextLine());
			if (!EmployeeDAO.getDAO().validationIsFree(bank, newID)) {
				System.out.println("That validation already exists. Try again with a new ID.");
				System.out.println("---------------------------------------------");
			} else {
				EmployeeDAO.getDAO().addVal(bank, newID);
			}
		} catch (SQLException e) {
			System.out.println("Error adding a new ID to the database.");
			System.out.println("---------------------------------------------");
			e.printStackTrace();
			return;		
		} catch (Exception e) {
			Menus.unknownChoiceMessage();
			System.out.println("---------------------------------------------");
			return;	
		}
		System.out.println("New employee ID validation code generated: " + newID);
		System.out.println("Give code to employee to sign up for a new employee account.");
		System.out.println("---------------------------------------------");
		} else {
			System.out.println("Sorry, only admins have permission to add a new employee ID.");
		}
	} 
}
