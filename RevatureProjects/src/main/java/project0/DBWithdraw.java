package project0;

import java.sql.SQLException;
import java.util.HashSet;

public class DBWithdraw {
	
	public static void withdraw(BankingMenuer bank) {
		//fetches accounts related to current customer
		HashSet<Integer> accounts;
		try {
			accounts = CustomerDAO.getDAO().getAccounts(bank);
		} catch (SQLException e) {
			System.out.println("Database error while fetching accounts.");
			e.printStackTrace();
			return;		
		}
		
		System.out.println("Available accounts:");
		System.out.println("--------------------------------------------");
		accounts.forEach( accid -> System.out.println("AccountID  --> " + accid));
		System.out.println("Enter the account ID from which you would like to withdraw:");
		int accid = 0;
		try {
			//gets amount to withdraw and makes the withdrawal
			accid = Integer.parseInt(bank.input.nextLine());
			float balance = AccountDAO.getDAO().getBalance(bank, accid);
			System.out.println("Current balance: $" + balance);
			System.out.println("---------------------------------------------");
			System.out.println("Enter the amount to withdraw: ");
			float amount = Float.parseFloat(bank.input.nextLine());
			//withdrawal call with false marking for whether this withdrawal was a transfer or not
			AccountDAO.getDAO().withdraw(bank, accid, amount, false);
			//logs the withdrawal from the account
			TransactionDAO.getDAO().logTransaction(bank, accid, "withdraw", amount);
		} catch (SQLException e) {
			System.out.println("Error while communicating with database.");
			System.out.println("Withdrawal failed.");
			System.out.println("--------------------------------------------");
			return;
		} catch (OverWithdrawalException e) {
			System.out.println("You are attempting to withdraw over your limit or more than your balance!");
			System.out.println("Withdrawal failed.");
			System.out.println("------------------------------------------------------------");
			return;
		} catch (NegativeDepositException e) {
			System.out.println("You can't withdraw a negative amount!");
			System.out.println("Withdrawal failed.");
			System.out.println("--------------------------------------------");
			return;
		} catch (Exception e) {
			Menus.unknownChoiceMessage();
			System.out.println("Withdrawal failed.");
			System.out.println("--------------------------------------------");
			return;
		}
		System.out.println("Withdrawal successful!");
		try {
			System.out.println("New balance: $"+AccountDAO.getDAO().getBalance(bank, accid));
		} catch (SQLException e) {
			System.out.println("Error displaying new balance.");
		}
		System.out.println("--------------------------------------------");
	}

}
