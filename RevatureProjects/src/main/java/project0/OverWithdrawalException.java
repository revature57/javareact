package project0;

public class OverWithdrawalException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OverWithdrawalException (String errorMessage) {
		super(errorMessage);
	}
}
