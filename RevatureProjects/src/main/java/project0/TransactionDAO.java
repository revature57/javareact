package project0;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionDAO {
	private static TransactionDAO singleton = null;
	
	public static TransactionDAO getDAO() {
		
		if (singleton == null) {
			singleton = new TransactionDAO();
		}
		
		return singleton;
	}
	
	public void logTransaction(BankingMenuer bank, int accid, String transaction, float amount) throws SQLException {
		
		String insertString = "INSERT INTO transactions (accountid, transaction, amount, transactionid) VALUES (? , ?, ?, DEFAULT)";
		PreparedStatement ps = bank.dbcon.prepareStatement(insertString);
		ps.setInt(1, accid);
		ps.setString(2, transaction);
		ps.setFloat(3, amount);
		ps.executeUpdate();
		ps.close();
		
		
	}

}
