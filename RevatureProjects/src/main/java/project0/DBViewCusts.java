package project0;

import java.util.HashSet;

public class DBViewCusts {
	public static void viewCustomer(BankingMenuer bank) {
		int custid = 0;
		System.out.println("Enter the customer ID to access their information.");
		try {
			custid = Integer.parseInt(bank.input.nextLine());
			String username = CustomerDAO.getDAO().getUsername(bank, custid);
			String password = CustomerDAO.getDAO().getPassword(bank, username);
			HashSet<Integer> accs = CustomerDAO.getDAO().getAccounts(bank, custid);
			System.out.println("Customer Information: ");
			System.out.println("---------------------------------------------------");
			System.out.println("Customer ID: " + custid);
			System.out.println("Customer username: " + username);
			System.out.println("Customer password: " + password);
			System.out.println("Customer Accounts: ");
			accs.forEach( accid -> System.out.println("AccountID  --> " + accid));
			if (bank.u instanceof Admin) {
				System.out.println("What would you like to do to this user?");
				System.out.println("---------------------------------------------------");
				System.out.println("1. Edit username");
				System.out.println("2. Edit password");
				String choice = bank.input.nextLine();
				switch (choice) {
				case "1":
					System.out.println("Enter new username: ");
					String newUsername = bank.input.nextLine();
					boolean usernameAvailable = !UserDAO.getDAO().userExists(bank, newUsername);
					if (usernameAvailable) {
						CustomerDAO.getDAO().setNewUsername(bank, custid, username, newUsername, password);
						System.out.println("New username set.");
						System.out.println("---------------------------------------------------");
					} else {
						System.out.println("That username is already taken.");
						System.out.println("Setting new username failed.");
						System.out.println("---------------------------------------------------");
					}
					break;
				case "2":
					System.out.println("Enter new password: ");
					String newPassword = bank.input.nextLine();
					CustomerDAO.getDAO().setNewPassword(bank, username, newPassword);
					System.out.println("New password set.");
					break;
				default:
					Menus.unknownChoiceMessage();
				}
			}
		} catch (Exception e) {
			Menus.unknownChoiceMessage();
		}
	}
}
