package project0;

public class DBManageAccounts {
	public static void manageAccounts(BankingMenuer bank) {
		System.out.println("Enter the account ID that you wish to view: ");
		int accID;
		boolean accExists;
		try {
			accID = Integer.parseInt(bank.input.nextLine());
			accExists = AccountDAO.getDAO().accountExists(bank, accID);
			
			if (accExists) {
				System.out.println("Account Information: ");
				System.out.println("---------------------------------------------------");
				System.out.println("Account ID: " + accID);
				System.out.println("Balance: $" + AccountDAO.getDAO().getBalance(bank, accID));
				System.out.println("Account maximum daily withdrawal: " + AccountDAO.getDAO().getMaxWithdrawal(bank, accID));
				System.out.println("---------------------------------------------------");
			} else {
				System.out.println("That account does not exist");
				System.out.println("---------------------------------------------------");
			} if (bank.u instanceof Admin && accExists) {
				System.out.println("What would you like to do with this account?");
				System.out.println("---------------------------------------------------");
				System.out.println("1. Edit account balance");
				System.out.println("2. Edit account maximum daily withdrawal");	
				System.out.println("3. Delete this account");	
				String choice = bank.input.nextLine();
				
				switch (choice) {
				case "1":
					System.out.println("Please enter the new balance:");
					String newBalance = bank.input.nextLine();
					try {
						AccountDAO.getDAO().setBalance(bank, accID, Float.parseFloat(newBalance));
					} catch (Exception e) {
						System.out.println("Input not understood.");
						System.out.println("Editing failed. Please try again.");	
						System.out.println("---------------------------------------------------");
					}
					break;
				case "2":
					System.out.println("Please enter the new withdrawal limit:");
					String newLimit = bank.input.nextLine();
					try {
						AccountDAO.getDAO().setMaxWithdrawal(bank, accID, Float.parseFloat(newLimit));
					} catch (Exception e) {
						System.out.println("Input not understood.");
						System.out.println("Editing failed. Please try again.");	
						System.out.println("---------------------------------------------------");
					}
					break;
				case "3":
					AccountDAO.getDAO().removeAcc(bank, accID);
					System.out.println("Account deleted successfully.");
					System.out.println("---------------------------------------------------");
					break;
				default:
					Menus.unknownChoiceMessage();
				}
			}
		} catch (Exception e) {
			
		}
	}
}
