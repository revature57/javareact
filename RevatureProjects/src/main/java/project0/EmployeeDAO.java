package project0;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EmployeeDAO {
	
	private static EmployeeDAO singleton = null;
	
	public static EmployeeDAO getDAO() {
		
		if (singleton == null) {
			singleton = new EmployeeDAO();
		}
		
		return singleton;
	}
	
	public int getEmpID(Connection con,String username) {
		
		int empid = 0;
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT employeeid FROM employees WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				empid = rs.getInt("employeeid");
			}
			ps.close();
		} catch (Exception e) {
				System.out.println("Error occurred while reading database.");
		}
		
		
		return empid;
	}
	
	public boolean validationIsFree(BankingMenuer bank, int validation) throws SQLException {
		boolean free = false;
		
		String checkVal = "SELECT COUNT(employeeid) FROM validations WHERE employeeid=?";
		String checkEmps = "SELECT COUNT(employeeid) FROM employees WHERE employeeid=?";
		
		int valCount = 0;
		int empCount = 0;
		PreparedStatement ps = bank.dbcon.prepareStatement(checkVal);
		ps.setInt(1, validation);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			valCount = rs.getInt("count(employeeid)");
			
		} else {
			throw new SQLException();
		}
		PreparedStatement ps1 = bank.dbcon.prepareStatement(checkEmps);
		ps1.setInt(1, validation);
		ResultSet rs1 = ps1.executeQuery();
		if (rs1.next()) {
			empCount = rs1.getInt("count(employeeid)");
		} else {
			throw new SQLException();
		}
//		System.out.println("valcount"+valCount);
//		System.out.println("empcount"+empCount);
		if (empCount < valCount || valCount == 0) {
			free = true;
		}
		rs.close();
		rs1.close();
		ps.close();
		ps1.close();
		
		return free;	
		
	}
	
	public void addEmployee(BankingMenuer bank, String username, String password, int empid) throws SQLException {		
		String type = "e";
		
		Statement stmnt = bank.dbcon.createStatement();
		stmnt.addBatch("INSERT INTO users (username, password, usertype) VALUES (\'"+username+"\',\'"+password+"\',\'"+type+"\')");
		stmnt.addBatch("INSERT INTO employees (username, employeeid) VALUES (\'"+username+"\',\'"+empid+"\')");
		stmnt.executeBatch();
		stmnt.close();
		
	}
	
	public void approve(BankingMenuer bank, int accid) throws SQLException {
		
		String approvalUpdate = "UPDATE cust_account SET approved = 1 WHERE accountid=?";
		PreparedStatement ps = bank.dbcon.prepareStatement(approvalUpdate);
		ps.setInt(1, accid);
		ps.executeUpdate();
		ps.close();
		return;
		
	}
	
	public void addVal(BankingMenuer bank, int newval) throws SQLException {
		
		PreparedStatement ps = bank.dbcon.prepareStatement("INSERT INTO validations (employeeid) VALUES (?)");
		ps.setInt(1, newval);
		int affectedRows = ps.executeUpdate();
		
		if (affectedRows == 0) {
			throw new SQLException("no rows affected");
		}
				
		ps.close();
		
		
	}

}
