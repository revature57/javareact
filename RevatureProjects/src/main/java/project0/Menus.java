package project0;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;


public class Menus {
	
	// Menu choices
	private static ArrayList<String> customerMenuChoices = new ArrayList<String>(
			Arrays.asList(
					"Deposit into accounts",
					"Withdraw from accounts",
					"Make a transfer",
					"Apply for a new account",
					"Quit" ));
	private static ArrayList<String> employeeMenuChoices = new ArrayList<String>(
			Arrays.asList(
					"View current accounts",
					"View pending accounts",
					"View current customers",
					"Add a validation for a new employee",
					"Quit" ));
	
	public static User login(BankingMenuer bank) {
		User u = null;
		//login menu switcher
		
		String choice = "null";
		boolean exit = false;
		while (!exit) {
						
			
			System.out.println("Welcome to Max Bank, please select a choice:");
			System.out.println("At any time using our app, enter a blank or nonsense to continue.");
			System.out.println("------------------------------------------------------------");
			System.out.println("1. Log in existing user");
			System.out.println("2. Sign up for a new user account");
			System.out.println("3. Quit");
			
			choice = bank.input.nextLine();
			
			switch(choice) {
			case "1":
				//choice made to log in
				String user;
				String pass;
				System.out.println("Enter Username: ");
				user = bank.input.nextLine();
				System.out.println("Enter Password: ");
				pass = bank.input.nextLine();
				// enable for default fileio
//				if (bank.files.userBase.keySet().contains(user+":"+pass)) {
//					u = bank.files.userBase.get(user+":"+pass);
//					exit = true;
//				} else {
//					System.out.println("Username or password incorrect, please try again. ");
//				}
				try {
					u = DatabaseManager.login(bank, user, pass);
				} catch (SQLException e) {
					System.out.println("Username or password incorrect, please try again. ");
					e.printStackTrace();
					continue;
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				if (u != null) {
					exit=true;
				}	
				break;
			case "2":
				//choice made to register for a new account
//				Requests.register(bank, bank.u);
				DBRegister.register(bank);				
				break;
			case "3":
				exit = true;
				break;
			default:
				unknownChoiceMessage();
			}
			
			
		}
		
		
		return u;
	}
	
	public static int customerMenu(BankingMenuer bank,User u) {
		//customer menu switcher
		String choice = "null";
		boolean exit = false;
		while (!exit) {
			System.out.println("Welcome Customer, what would you like to do today?");
			System.out.println("Customer ID:" + ((Customer) u).getCustomerID());
			System.out.println("-------------------------------------------------------");
			for (int i = 0; i<customerMenuChoices.size(); i++) {
				System.out.println(Integer.toString(i+1)+". "+customerMenuChoices.get(i));
			}
			
			choice = bank.input.nextLine();
			
			switch (choice) {
			case "1":
				//choice made to deposit
//				Requests.custDepositMenu(bank,u);
				DBDeposit.deposit(bank);
				break;
			case "2":
				//choice made to withdraw
//				Requests.custWithdrawMenu(bank,u);
				DBWithdraw.withdraw(bank);
				break;
			case "3":
				//choice made to transfer
//				Requests.custTransferMenu(bank,u);
				DBTransfer.transfer(bank);
				break;
			case "4":
				//choice made to apply for a new account
//				Requests.custApplyMenu(bank,u);
				DBApply.newAccount(bank);
				break;
			case "5":
				exit = true;
				break;
			default:
				unknownChoiceMessage();
			} 
			
		}
		
		return 1;
	}
	
	public static void unknownChoiceMessage() {
		//message to be displayed on invalid input
		System.out.println("Sorry, we don't understand that input. Please try again. "
				+ "\n "
				+ "-----------------------------------------------------------------");
	}


	public static int employeeMenu(BankingMenuer bank,User u) {
		//employee menu switcher
		String choice = "null";
		boolean exit = false;
		while (!exit) {
			
			if (u instanceof Admin) {
				System.out.println("Welcome Admin, what would you like to do today?");
			} else {
				System.out.println("Welcome Employee, what would you like to do today?");
			}
			System.out.println("-------------------------------------------------------");
			for (int i = 0; i<employeeMenuChoices.size(); i++) {
				System.out.println(Integer.toString(i+1)+". "+employeeMenuChoices.get(i));
			}
			
			choice = bank.input.nextLine();
			
			switch (choice) {
			case "1":
				//choice made to view and manage accounts
//				Requests.empViewAccMenu(bank,u);
				DBManageAccounts.manageAccounts(bank);
				break;
			case "2":
				//choice made to view pending accounts and approve one
//				Requests.empViewPendMenu(bank,u);
				DBPending.viewPending(bank);
				break;
			case "3":
				//choice made to view and manage customer profiles
//				Requests.empViewCustsMenu(bank,u);
				DBViewCusts.viewCustomer(bank);
				break;
			case "4":
				//choice made to add a new code for an employee
//				Requests.empAddValMenu(bank, u);
				DBAddVal.addVal(bank);
				break;
			case "5":
				exit = true;
				break;
			default:
				unknownChoiceMessage();
			} 
			
		}
		
		
		return 2;
	}
	
	
}
