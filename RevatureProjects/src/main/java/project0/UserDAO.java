package project0;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {
	
	private static UserDAO singleton = null;
	
	public static UserDAO getDAO() {
		
		if (singleton == null) {
			singleton = new UserDAO();
		}
		
		return singleton;
	}
	
	public boolean userExists(BankingMenuer bank, String username) throws SQLException {
		
		boolean exists = false;
		
		String checkUser = "SELECT COUNT(username) FROM users WHERE username=?";
		
		PreparedStatement ps = bank.dbcon.prepareStatement(checkUser);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		rs.next();
		int count = rs.getInt("COUNT(username)");
		if (count > 0) {
			exists = true;
		}
		rs.close();
		ps.close();
			
		return exists;
	}
	
	

}
