package project0;

import java.io.Serializable;
import java.util.ArrayList;

public class Customer extends User implements Serializable{
	// class for customer accounts
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7727587607971491282L;
	private String customerID;
	private ArrayList<String> accountIDs = new ArrayList<String>();
	
	public Customer() {
		// null customer for testing
		this.username = "custUser";
		this.password = "custPass";
		this.customerID = "custID";
		this.accountIDs.add("accID1");
	}
	
	public Customer(String user, String pass, String cID) {
		this.username = user;
		this.password = pass;
		this.customerID = cID;
	}
	
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public ArrayList<String> getAccounts() {
		return accountIDs;
	}
	public void setAccounts(ArrayList<String> accounts) {
		this.accountIDs = accounts;
	}
	
	@Override
	@Deprecated
	public String stringify() {
		StringBuffer dataString = new StringBuffer();
		
		dataString.append(username + ":" + password + ",");
		dataString.append("Customer,");
		dataString.append(customerID+",");
		accountIDs.forEach(str -> dataString.append(str+","));
		dataString.append("end\n");
		
		return dataString.toString();
	}
	
	@Deprecated
	public static User parseString(ArrayList<String> parsed) {
		String[] loginInfo = parsed.get(0).split(":");
    	User cust = new Customer(loginInfo[0], loginInfo[1], parsed.get(2));
//    	for (String str:parsed) {
//    		System.out.println(str);
//    	}
    	
    	int i = 3;
    	while (!parsed.get(i).equals("end")) {
    		((Customer) cust).getAccounts().add(parsed.get(i));
    		i++;
    	}
    	return cust;
	}

}
