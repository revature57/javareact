package project0;

import java.io.Serializable;
import java.util.ArrayList;

public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4952287072690419237L;
	// superclass for individual and joint checking accounts
	private String accountID;
	private float balance;
//	private float balanceLimit;
	private float maxWithdrawal;
	private boolean isPending;
	private float withdrawn;
	
	
//	public float getBalanceLimit() {
//		return balanceLimit;
//	}
//	Account acc = new Account();
//	Account acc2 = new Account();
//	
//
//	public void setBalanceLimit(float balanceLimit) {
//		this.balanceLimit = balanceLimit;
//	}
	
	public float getWithdrawn() {
		return withdrawn;
	}

	
	public void setWithdrawn(float withdrawn) {
		this.withdrawn = withdrawn;
	}


	public boolean isPending() {
		return isPending;
	}


	public void setPending(boolean isPending) {
		this.isPending = isPending;
	}


	public Account() {
		super();
		this.accountID = "accID0";
		this.balance = 0f;
		this.maxWithdrawal = 500f;
		this.withdrawn = 0f;
		this.isPending = false;
	}


	public Account(String accountID, float balance, float maxWithdrawal, boolean isP) {
		super();
		this.accountID = accountID;
		this.balance = balance;
		this.maxWithdrawal = maxWithdrawal;
		this.isPending = isP;
		this.withdrawn = 0f;
	}
	


	public float getMaxWithdrawal() {
		return maxWithdrawal;
	}


	public void setMaxWithdrawal(float maxWithdrawal) throws NegativeDepositException {
		if (maxWithdrawal < 0) {
			throw new NegativeDepositException("Can't have a negative withdrawal limit!");
		}
		this.maxWithdrawal = maxWithdrawal;
	}


	public String getAccountID() {
		return accountID;
	}


	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}


	public float getBalance() {
		return balance;
	}


	public void setBalance(float balance) throws NegativeDepositException {
		if (balance < 0) {
			throw new NegativeDepositException("Error negative balance entered.");
		}
		this.balance = balance;
	}


	public Account(String accountID) {
		this.accountID = accountID;
	}
	
	@Deprecated
	public String stringify() {
		//turns an account into a csv string
		StringBuffer dataString = new StringBuffer();
		
		dataString.append(this.accountID+",");
		dataString.append(this.balance+",");
		dataString.append(this.maxWithdrawal+",");
//		dataString.append(this.balanceLimit+",");
		dataString.append(isPending+",");
		dataString.append("end\n");
		
		return dataString.toString();
	}
	
	@Deprecated
	public static Account parseString(ArrayList<String> parsed) {
		//turns an account string back into an account
		return new Account(parsed.get(0), Float.parseFloat(parsed.get(1)), Float.parseFloat(parsed.get(2)), Boolean.parseBoolean(parsed.get(3)));
	}
	
	
	public void withdraw(float amount) throws NegativeDepositException, OverWithdrawalException {	
		//checks for error in amount passed, then does transaction
		if (amount < 0) {
			throw new NegativeDepositException("Tried to withdraw negative amount!");
		} else if ( amount > this.balance || amount + this.withdrawn > this.maxWithdrawal) {
			throw new OverWithdrawalException("Tried to withdraw too much!");
		}
		this.withdrawn += amount;
		this.balance -= amount;
		System.out.println("$"+amount+" withdrawn successfully.");
		System.out.println("New balance: $" + this.balance);
		
	}
	
	public void deposit(float amount) throws NegativeDepositException {
		//checks for error in amount passed, then does transaction
		if (amount < 0) {
			throw new NegativeDepositException("Tried to deposit negative amount!");
		}
		
		this.balance+=amount;
		System.out.println("$"+amount+" deposited successfully.");
		System.out.println("New balance: $" + this.balance);
		
	}
	
//	public void transfer() {
//		//TODO
//		
//	}
}
