package project0;

import java.sql.*;
import java.util.HashSet;

public class CustomerDAO {
	private static CustomerDAO singleton = null;
	
	public static CustomerDAO getDAO() {
		
		if (singleton == null) {
			singleton = new CustomerDAO();
		}
		
		return singleton;
	}
	
	public int getCustID(Connection con,String username) {
		
		int custid = 0;
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT custid FROM customers WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				custid = rs.getInt("custid");
			}
			ps.close();
		} catch (Exception e) {
				System.out.println("Error occurred while reading database.");
		}
		
		
		return custid;
	}
	
	public void addCustomer(BankingMenuer bank, String username, String password) throws SQLException {		
		String type = "c";
		Statement stmnt = bank.dbcon.createStatement();
		stmnt.addBatch("INSERT INTO users (username, password, usertype) VALUES (\'"+username+"\',\'"+password+"\',\'"+type+"\')");
		stmnt.addBatch("INSERT INTO customers (username, custid) VALUES (\'"+username+"\', DEFAULT)");
		stmnt.executeBatch();
		stmnt.close();
	}
	
	public HashSet<Integer> getAccounts(BankingMenuer bank) throws SQLException {
		
		HashSet<Integer> accounts = new HashSet<Integer>();
		String getAccountQuery = "SELECT accountid FROM cust_account WHERE (custid=? AND approved=?)";
		PreparedStatement ps = bank.dbcon.prepareStatement(getAccountQuery);
		ps.setInt(1, Integer.parseInt(((Customer) bank.u).getCustomerID()));
		ps.setInt(2, 1);
		ResultSet rs = ps.executeQuery();
		
		if (!rs.next()) {
			return accounts;
		} else {
			do {
				accounts.add(rs.getInt("accountid"));
			} while (rs.next());
		}
		
		rs.close();
		ps.close();
		
		return accounts;
		
	}
	
	public HashSet<Integer> getAccounts(BankingMenuer bank, int custid) throws SQLException {
		
		HashSet<Integer> accounts = new HashSet<Integer>();
		String getAccountQuery = "SELECT accountid FROM cust_account WHERE (custid=? AND approved=?)";
		PreparedStatement ps = bank.dbcon.prepareStatement(getAccountQuery);
		ps.setInt(1, custid);
		ps.setInt(2, 1);
		ResultSet rs = ps.executeQuery();
		
		if (!rs.next()) {
			return accounts;
		} else {
			do {
				accounts.add(rs.getInt("accountid"));
			} while (rs.next());
		}
		
		rs.close();
		ps.close();
		
		return accounts;
		
	}
	
	public String getPassword(BankingMenuer bank, String username) {
		String password = null;
		
		try {
			PreparedStatement ps = bank.dbcon.prepareStatement("SELECT password FROM users WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				password = rs.getString("password");
			}
			ps.close();
		} catch (Exception e) {
				System.out.println("Error occurred while reading database.");
		}
		
		
		return password;
	}
	
	public String getUsername(BankingMenuer bank, int custid) {
		String username = null;
		
		try {
			PreparedStatement ps = bank.dbcon.prepareStatement("SELECT username FROM customers WHERE custid=?");
			ps.setInt(1, custid);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				username = rs.getString("username");
			}
			ps.close();
		} catch (Exception e) {
				System.out.println("Error occurred while reading database.");
		}
				
		return username;
	}
	
	public void setNewUsername(BankingMenuer bank, int custid, String username, String newusername, String password) throws SQLException {
		
		String setUser = "INSERT INTO users (username, password, usertype) VALUES (? , ?, ?)";
		String updateUser = "UPDATE customers SET username=? WHERE custid=?";
		
		PreparedStatement ps1 = bank.dbcon.prepareStatement(setUser);
		ps1.setString(1, newusername);
		ps1.setString(2, password);
		ps1.setString(3, "c");
		ps1.executeUpdate();
		ps1.close();
		PreparedStatement ps2 = bank.dbcon.prepareStatement(updateUser);
		ps2.setString(1, newusername);
		ps2.setInt(2, custid);
		ps2.executeUpdate();
		ps2.close();
		PreparedStatement ps3 = bank.dbcon.prepareStatement("DELETE FROM users WHERE username=?");
		ps3.setString(1, username);
		ps3.executeUpdate();
		ps3.close();
	}
	
	public void setNewPassword(BankingMenuer bank, String username, String newpassword) throws SQLException {
		
		String updateUser = "UPDATE users SET password=? WHERE username=?";
		
		PreparedStatement ps1 = bank.dbcon.prepareStatement(updateUser);
		ps1.setString(1, newpassword);
		ps1.setString(2, username);
		ps1.executeUpdate();
		ps1.close();

	}


}
