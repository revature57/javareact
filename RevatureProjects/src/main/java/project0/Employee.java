package project0;

import java.io.Serializable;
import java.util.ArrayList;

public class Employee extends Customer implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5483543967801628473L;
	private String employeeValidation;
	
	public Employee() {
		this.username = "employUser";
		this.password = "employPass";
		this.employeeValidation = "employVal";
	}
	public Employee(String user, String pass, String empVal) {
		this.username = user;
		this.password = pass;
		this.employeeValidation = empVal;
	}
	
	public String getEmployeeValidation() {
		return employeeValidation;
	}
	public void setEmployeeValidation(String employeeValidation) {
		this.employeeValidation = employeeValidation;
	}
	
	@Override
	@Deprecated
	public String stringify() {
		StringBuffer dataString = new StringBuffer();
				
		dataString.append(username + ":" + password + ",");
		dataString.append("Employee,");
		dataString.append(employeeValidation+",");
		dataString.append("end\n");
		
		return dataString.toString();
		
	}
	
	@Deprecated
	public static User parseString(ArrayList<String> parsed) {
		String[] loginInfo = parsed.get(0).split(":");
		User employee = new Employee(loginInfo[0], loginInfo[1], parsed.get(2));
		return employee;
	}
	
}
